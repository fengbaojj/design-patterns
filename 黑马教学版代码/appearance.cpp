#if 0
#include<iostream>
using namespace std;
//外观模式，即提供一组具有类似功能的类群，如子系统
//将复杂子系统归到一个类里
//案例：实现一个可切换模式的家庭影院

//也可以写一个物品的基类，这里犯懒没有写
class TV {
public:
	void on() {
		cout << "电视机打开" << endl;
	}
	void off() {
		cout << "电视机关闭" << endl;
	}
};

class Light {
public:
	void on() {
		cout << "灯打开" << endl;
	}
	void off() {
		cout << "灯关闭" << endl;
	}
};

class Audio {
public:
	void on() {
		cout << "音响打开" << endl;
	}
	void off() {
		cout << "音响关闭" << endl;
	}
};

class Microphone {
public:
	void on() {
		cout << "麦克风打开" << endl;
	}
	void off() {
		cout << "麦克风关闭" << endl;
	}
};

class DVD {
public:
	void on() {
		cout << "DVD打开" << endl;
	}
	void off() {
		cout << "DVD关闭" << endl;
	}
};

class gamePlayer {
public:
	void on() {
		cout << "游戏机打开" << endl;
	}
	void off() {
		cout << "游戏机关闭" << endl;
	}
};

class KTVMode {
public:
	KTVMode() {
		tv = new TV;
		light = new Light;
		audio = new Audio;
		microphone = new Microphone;
		dvd = new DVD;
	}

	void onKTV() {
		tv->on();
		light->off();
		audio->on();
		microphone->on();
		dvd->on();
	}

	void offKTV() {
		//和上述onKTV()的功能整体相反即可
	}

	~KTVMode() {
		delete tv, light, audio, microphone, dvd;
		//竟然可以写在一起，我也很惊讶
		//但还是分开写比较规范
		//还没有查C++标准，这可能只是VS的一种实现
	}
private:
	TV* tv;
	Light* light;
	Audio* audio;
	Microphone* microphone;
	DVD* dvd;
};

int main() {
	KTVMode* ktv = new KTVMode;
	ktv->onKTV();
}
#endif