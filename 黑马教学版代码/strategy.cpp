#if 0
#include<iostream>
using namespace std;
//策略模式，定义一系列算法，并将其封装起来
//策略模式让算法独立于使用它的客户而独立变化

class weaponStrategy {
public:
	virtual void useWeapon() = 0;
};

class Knife :public weaponStrategy {
public:
	void useWeapon() override{
		cout << "使用匕首" << endl;
	}
};

class AK47 :public weaponStrategy{
public:
	void useWeapon() override {
		cout << "使用AK47" << endl;
	}
};

class Charater {
public:
	void setWeapon(weaponStrategy* w) {
		weapon = w;
	}

	void wieldWeapon() {
		weapon->useWeapon();//与角色类无关
	}
private:
	weaponStrategy* weapon;
};

int main() {
	Charater* stormwine = new Charater;

	weaponStrategy* knife = new Knife;
	weaponStrategy* ak47 = new AK47;

	stormwine->setWeapon(knife);
	stormwine->wieldWeapon();

	stormwine->setWeapon(ak47);
	stormwine->wieldWeapon();

	delete stormwine, knife, ak47;
}
#endif