#if 0
#include<iostream>
using namespace std;

//代理模式，提供一个代理控制其他对象访问
class abstractCommonInterface {
public:
	virtual void run() = 0;
};
//抽象接口类，是各种系统的基类

class mySystem :public abstractCommonInterface {
public:
	void run() override {
		cout << "系统已启动" << endl;
	}
};
//某个实例化好了的系统

//必须要有权限验证，提供用户名和密码
class mySystemProxy :public abstractCommonInterface {
public:
	mySystemProxy(string name, string password):userName(name),userPassword(password){
		system = new mySystem;
	}

	void run() {
		if (checkUserNameAndPassword())
		{
			cout << "账号密码正确！" << endl;
			system->run();
		}
		else
			cout << "用户名或密码错误！" << endl;
	}

	bool checkUserNameAndPassword() {
		if (userName == "admin" && userPassword == "123456") return true;
		return false;
	}

	~mySystemProxy() {
		if (system != nullptr) delete system;
	}
public:
	mySystem* system;
	string userName;
	string userPassword;
};

int main() {
	mySystemProxy* system = new mySystemProxy("root","123456");
	system->run();
}
#endif