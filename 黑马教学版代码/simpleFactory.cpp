#if 0
#include<iostream>
using namespace std;
//简单工厂模式，做水果

class abstractFruit {
public:
	virtual void showName() = 0;
};

class Apple :public abstractFruit {
public:
	void showName() override{
		cout << "我是苹果！" << endl;
	}
};

class Banana :public abstractFruit {
public:
	void showName() override {
		cout << "我是香蕉！" << endl;
	}
};

class Coconut :public abstractFruit {
public:
	void showName() override {
		cout << "我是椰子！" << endl;
	}
};

class fruitFactory {
public:
	static abstractFruit* createFruit(string type) {
		if (type == "apple") {
			return new Apple;
		}
		else if (type == "banana") {
			return new Banana;
		}
		else if (type == "coconut") {
			return new Coconut;
		}
		else {
			cout << type + "不是一个水果类型" << endl;
			return nullptr;
		}
	}//也可以不写作静态方法而写成单例模式
};
//简单工厂通过一个类去创造新的对象
//具体函数依然可以如依赖倒转原则一样通过基类调用

void test() {
	fruitFactory* factory = new fruitFactory;
	abstractFruit* fruit = factory->createFruit("apple");
	fruit->showName();
	delete fruit;

	fruit = factory->createFruit("banana");
	fruit->showName();
	delete fruit;

	fruit = factory->createFruit("coconut");
	fruit->showName();
	delete fruit;

	delete factory;
}

int main() {
	test();
}
#endif