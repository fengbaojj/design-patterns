#if 0
#include<iostream>
using namespace std;
//单例模式，整个系统中只有一个当前类的对象

class single {
public:
	static single* getInstance() {
		return onlyOne;
	}
	~single() {
		delete onlyOne;
	}
private:
	single(){
		onlyOne = new single;
	}
	//构造函数私有化
private:
	static single* onlyOne;
};

single* single::onlyOne = nullptr;

//实现单例
// 1.构造函数私有化
// 2.增加静态私有类指针
// 3.静态对外接口，让用户获得单例对象

//单例创建分为懒汉式和饿汉式

//懒汉式，创建时才会调用对象
//遇到多线程时会有线程安全问题，尽量不用
class lazySingleton {
public:
	~lazySingleton() {
		if (lazy != nullptr) delete lazy;
	}
	static lazySingleton* getInstance() {
		if (lazy == nullptr) {
			lazy = new lazySingleton;
		}

		return lazy;
	}
private:
	lazySingleton(){
		cout << "懒" << endl;
	}
private:
	static lazySingleton* lazy;
};

lazySingleton* lazySingleton::lazy = nullptr;

//饿汉式
class hungrySingleton {
public:
	~hungrySingleton() {
		delete hungry;
	}
	static hungrySingleton* getInstance() {
		return hungry;
	}

	class Garbo {
		~Garbo() {
			if (hungry != nullptr) {
				delete hungry;
			}
		}
	};
	//嵌套类，闻所未闻，用于释放单例对象
	//实际上由于单例对象不会造成溢出（始终只有一份）
	//因此不释放对程序运行影响也不大
private:
	hungrySingleton() {
		cout << "饿" << endl;
	}
private:
	static hungrySingleton* hungry;
	static Garbo garbo;
};

hungrySingleton* hungrySingleton::hungry = new hungrySingleton;


void test() {
	lazySingleton* lazy1 = lazySingleton::getInstance();
	lazySingleton* lazy2 = lazySingleton::getInstance();
	if (lazy1 == lazy2) {
		cout << "单例" << endl;
	}
	else {
		cout << "不是单例" << endl;
	}

	hungrySingleton* hungry1 = hungrySingleton::getInstance();
	hungrySingleton* hungry2 = hungrySingleton::getInstance();
	if (hungry1 == hungry2) {
		cout << "单例" << endl;
	}
	else {
		cout << "不是单例" << endl;
	}
}

int main() {
	test();
}
#endif