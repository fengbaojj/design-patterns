#if 0
#include<iostream>
using namespace std;
//抽象工厂，生产水果
// 实际生活中可能实际产品有所不同
// 如果相同的话，还是产品写到一起，然后多个工厂就好
//针对产品族符合开闭原则（实例中为一国）

class abstractApple {
public:
	virtual void showName() = 0;
};

class ChinaApple :public abstractApple {
public:
	void showName() override {
		cout << "中国苹果" << endl;
	}
};

class JapanApple :public abstractApple {
public:
	void showName() override {
		cout << "日本苹果" << endl;
	}
};

class AmericaApple :public abstractApple {
public:
	void showName() override {
		cout << "美国苹果" << endl;
	}
};

class abstractBanana {
public:
	virtual void showName() = 0;
};

class ChinaBanana :public abstractBanana {
public:
	void showName() override {
		cout << "中国香蕉" << endl;
	}
};

class JapanBanana :public abstractBanana {
public:
	void showName() override {
		cout << "日本香蕉" << endl;
	}
};

class AmericaBanana :public abstractBanana {
public:
	void showName() override {
		cout << "美国香蕉" << endl;
	}
};

class abstractCoconut {
public:
	virtual void showName() = 0;
};

class ChinaCoconut :public abstractCoconut {
public:
	void showName() override {
		cout << "中国椰子" << endl;
	}
};

class JapanCoconut :public abstractCoconut {
public:
	void showName() override {
		cout << "日本椰子" << endl;
	}
};

class AmericaCoconut :public abstractCoconut {
public:
	void showName() override {
		cout << "美国椰子" << endl;
	}
};

//抽象工厂，针对产品族
//为了一个工厂可生成多个产品
class abstractFactory {
public:
	virtual abstractApple* createApple() = 0;
	virtual abstractBanana* createBanana() = 0;
	virtual abstractCoconut* createCoconut() = 0;
};
// 这里定义成为纯虚函数其实破坏了产品族的开闭原则
// 每当加入一个新的产品结构，必须进到原有工厂修改代码
// 否则无法创建实例对象，这里感觉有些问题
// 待我修改案例

class ChinaFactory :public abstractFactory {
	abstractApple* createApple() override{
		return new ChinaApple;
	}
	abstractBanana* createBanana() override {
		return new ChinaBanana;
	}
	abstractCoconut* createCoconut() override {
		return new ChinaCoconut;
	}
};

class AmericaFactory :public abstractFactory {
	abstractApple* createApple() override {
		return new AmericaApple;
	}
	abstractBanana* createBanana() override {
		return new AmericaBanana;
	}
	abstractCoconut* createCoconut() override {
		return new AmericaCoconut;
	}
};

class JapanFactory :public abstractFactory {
	abstractApple* createApple() override {
		return new JapanApple;
	}
	abstractBanana* createBanana() override {
		return new JapanBanana;
	}
	abstractCoconut* createCoconut() override {
		return new JapanCoconut;
	}
};

void test() {
	abstractFactory* factory = nullptr;
	abstractApple* apple = nullptr;
	abstractBanana* banana = nullptr;
	abstractCoconut* coconut = nullptr;

	factory = new ChinaFactory;
	apple = factory->createApple();
	banana = factory->createBanana();
	coconut = factory->createCoconut();

	apple->showName();
	banana->showName();
	coconut->showName();

	delete factory;
	delete apple;
	delete banana;
	delete coconut;
}

int main() {
	test();
}
#endif